import os
import commands
from contextlib import contextmanager
import subprocess

@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)


#home_dir = raw_input('Server Username: ')
home_dir = commands.getoutput('pwd').split('/')[2]
cmd = "sudo docker exec helium_postgres_1 bash -lc 'pg_dump -U postgres -Fc onemedical' > /home/{}/heliumbkp/dump_`date +%d-%m-%Y\"_\"%H_%M_%S`.bak" .format(home_dir)


try:
    root, dirs, files = os.walk("/media/{}" .format(home_dir)).next()
    drive = [i for i in dirs if not '.' in i and not ' ' in i][-1]
    #drive = raw_input('Enter External Hard Drive name: ')
    print '********************** \n', drive , '\n***********************'
    #cmd1 = "sudo docker exec helium_postgres_1 bash -lc 'pg_dump -U postgres -Fc onemedical' > /media/{home_dir}/{drive}/heliumbkp/dump_`date +%d-%m-%Y\"_\"%H_%M_%S`.bak" .format(home_dir=home_dir, drive=drive)
    cmd1 = 'sudo rsync -ar /home/{home_dir}/heliumbkp /media/{home_dir}/{drive}' .format(home_dir=home_dir, drive=drive)
    with cd('/media/{home_dir}/{drive}' .format(home_dir=home_dir, drive=drive)):
        os.system('pwd')
        os.system('sudo rm -r heliumbkp')
        os.system('sudo rm -r uploads')
        #os.system('mkdir heliumbkp')
except Exception as e:
    cmd1 = ''
    print e
    print 'No Backup External Hard drive found'
    pass


def action(cmd, cmd1):
    script_lines = """
#!/bin/bash

{cmd}
{cmd1}

    """ .format(cmd=cmd, cmd1=cmd1)

    return script_lines.split('\n')


def get_bash_file(path, sh):
    script_lines = """
#!/bin/bash
sudo crontab -l > backup_cron
echo " */5 * * * * cd {path} && ./{sh}" >> backup_cron
crontab backup_cron
rm backup_cron
    """ .format(path=path, sh=sh)

    return script_lines.split('\n')

def trim_bash(home_dir):
    script_lines = """
#!/bin/bash
sudo crontab -l > backup_cron
echo " 0 */12 * * * cd /home/{}/heliumbkp && sudo  python trim_backups.py" >> backup_cron
crontab backup_cron
rm backup_cron
    """ .format(home_dir)

    return script_lines.split('\n')


def uploads_bash(path, home_dir,  drive):
    script_lines = """
#!/bin/bash
sudo crontab -l > backup_cron
echo " 0 */12 * * * sudo rsync -ar /home/{home_dir}/helium/uploads /media/{home_dir}/{drive}" >> backup_cron
crontab backup_cron
rm backup_cron
    """ .format(path=path, home_dir=home_dir, drive=drive)

    return script_lines.split('\n')



# try:
#     with cd('/media/{home_dir}/{drive}' .format(home_dir=home_dir, drive=drive)):
#         os.system('mkdir heliumbkp')
# except:
#     pass

try:
    os.system('sudo apt-get install rsync')
except Exception  as e:
    print(e)

try:
    with cd('/home/%s/' % home_dir):
        os.system('mkdir heliumbkp')
        bkp = commands.getoutput('pwd')
        with cd(bkp + '/heliumbkp/'):
            bsh = open('script.sh', 'w')
            for line in action(cmd, cmd1):
                bsh.write('\n')
                bsh.write(line)
            bsh.close()
            os.system('sudo chmod +x script.sh')
            bash = open('backup_crontab.sh', 'w')
            path_to = commands.getoutput('pwd')
            for line in get_bash_file(path_to, 'script.sh'):
                print line
                bash.write('\n')
                bash.write(line)
                bash.write('\n')
            bash.close()
            os.system('pwd')
            os.system('sudo chmod +x backup_crontab.sh')
            os.system('./backup_crontab.sh')
            os.system('sudo curl https://gitlab.com/Kossyb20/helium/raw/master/trim_backups.py -O')


    print 'DB Backup Setup Successfully!'

except Exception  as e:
    print 'Something has gone terribly wrong!.', e




try:
    with cd('/home/%s/heliumbkp' % home_dir):
        bsh = open('trim_backups.sh', 'w')
        for line in trim_bash(home_dir):
            bsh.write('\n')
            bsh.write(line)
        bsh.close()
        os.system('sudo chmod +x trim_backups.sh')
        os.system('./trim_backups.sh')

    print 'Trimming Backups Setup Successfully!'

except Exception as e:
    print 'Something has gone terribly wrong!. ', e 





try:
    with cd('/home/%s/helium' % home_dir):
        
        path = '/home/%s/helium' % home_dir
        bsh = open('uploads_backup.sh', 'w')
        for line in uploads_bash(path, home_dir, drive):
            bsh.write('\n')
            bsh.write(line)
        bsh.close()
        os.system('sudo chmod +x uploads_backup.sh')
        os.system('./uploads_backup.sh')

    print 'Uploads Backup Setup Successfully!'

except Exception as e:
    print 'Something has gone terribly wrong!. ', e 
