import os
import commands
from contextlib import contextmanager


@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)



print "\n\n\n\n\n\n ****************** \n\n\n\n\n\n\n\n Installing Docker  \n\n\n\n\n\n ****************** \n\n\n\n\n\n\n\n "
try:
    os.system('curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -')
    os.system('sudo add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"')
    os.system('sudo apt-get update')
    os.system('apt-cache policy docker-ce')
    os.system('sudo apt-get install -y docker-ce')
    os.system('sudo systemctl status docker')
    version = commands.getoutput('docker --version')
    #os.system('docker')
    print " \n\n\n\n\n\n ****************** \n\n\n\n\n\n\n\n Successfully installed " + version + " \n\n\n\n\n\n ****************** \n\n\n\n\n\n\n\n "

except Exception as e:
    print " \n\n\n\n\n\n ****************** \n\n\n\n\n\n\n\n  This terrible thing happened: ", e, '\n\n\n\n\n\n ****************** \n\n\n\n\n\n\n\n' 
    

#install Docker Compose
print " \n\n\n\n\n\n ****************** \n\n\n\n\n\n\n\n Installing Docker Compose \n\n\n\n\n\n ****************** \n\n\n\n\n\n\n\n "

try:

    os.system('sudo curl -L https://github.com/docker/compose/releases/download/1.18.0/docker-compose-`uname -s`-`uname -m` -o /usr/local/bin/docker-compose')
    os.system('sudo chmod +x /usr/local/bin/docker-compose')
    os.system('docker-compose --version')
    version = commands.getoutput('docker-compose --version')

    print " \n\n\n\n\n\n ****************** \n\n\n\n\n\n\n\n  Successfully installed Docker \n\n\n\n\n\n ****************** \n\n\n\n\n\n\n\n "

except Exception as e:
    print " \n\n\n\n\n\n ****************** \n\n\n\n\n\n\n\n  This terrible thing happened: ", e, '\n\n\n\n\n\n ****************** \n\n\n\n\n\n\n\n '
    


