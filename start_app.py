import os
import commands
from contextlib import contextmanager


@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)

home_dir = raw_input('Server Username: ')


def action():
    script_lines = """
#!/bin/bash
sudo su
docker-compose down

docker-compose up -d

    """ 

    return script_lines.split('\n')


with cd('/home/%s/helium' % home_dir):

    bsh = open('script.sh', 'w')
    for line in action():
        bsh.write('\n')
        bsh.write(line)
    bsh.close()
    os.system('sudo chmod +x script.sh')
    os.system('./script.sh')

print " No errors. Check if software is up"