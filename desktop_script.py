import os
import commands
from contextlib import contextmanager
import subprocess

@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)


def action(version):
    script_lines = """
#!/bin/bash

cd /home/ubuntu/helium && env VERSION={version} docker-compose down
cd /home/ubuntu/helium && env VERSION={version} docker-compose up -d
    """ .format(version=version)

    return script_lines.split('\n')



try:
    version = str(raw_input('Enter version example 3.8.6 : '))
    home_dir = commands.getoutput('pwd').split('/')[2]
    with cd('/home/{}/Desktop'.format(home_dir)):
        os.system('pwd')
        with open('Start_Software.sh', 'w') as config:
            for line in action(version):
                config.write(line)
                config.write('\n')
    
        os.system('sudo chmod +x Start_Software.sh')


except Exception as e:
    print e









