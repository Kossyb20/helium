import os
import commands
from contextlib import contextmanager


@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)



# Setup helium health

print " \n\n\n\n\n\n ****************** \n\n\n\n\n\n\n\n  Setting up helium health \n\n\n\n\n\n ****************** \n\n\n\n\n\n\n\n "



try:
    version = str(raw_input('Enter version example 3.8.6 : '))
    token = str(raw_input('Enter hospital token: '))

    # 'UNIVERSE_TOKEN=pbWTQJpcHb3TC_dqByM9ZsALyXo'


    home_dir = commands.getoutput('pwd').split('/')[2]

    with cd('/home/%s/' % home_dir):
        os.system('mkdir helium')
        with cd('/home/%s/helium' % home_dir):
            os.system('curl https://gitlab.com/Kossyb20/helium/raw/master/ops.zip -O')
            os.system('sudo apt install unzip')
            os.system('unzip ops.zip')
            os.system('cp /home/{home}/helium/ops/docker-compose.yml /home/{home_d}/helium/docker-compose.yml'.format(home=home_dir, home_d=home_dir))
            config = open('config.env', 'w')
            config.write('UNIVERSE_TOKEN={}'.format(token))
            config.close()
            os.system('env VERSION={} docker-compose up -d'.format(version))
            os.system('chown -R nobody prometheus')
            os.system('chown -R 472 grafana')
            os.system('env VERSION={} docker-compose ps'.format(version))
            os.system('wget https://dl.minio.io/client/mc/release/linux-amd64/mc')
            os.system('chmod +x mc')
            os.system('sudo mv mc /usr/local/bin')
            os.system('mc config host add helium http://localhost:9000')
            os.system('mc mb --region=eu-west-1 helium/upload')

    ip = commands.getoutput('hostname -I').split(' ')[0]
    url = str(ip) + ':8080'

    print " \n\n\n\n\n\n ****************** \n\n\n\n\n\n\n\n  Setup successful. Try running app \n\n\n\n\n\n ****************** \n\n\n\n\n\n\n\n "



except Exception as e:
    print " \n\n\n\n\n\n ****************** \n\n\n\n\n\n\n\n This terrible thing happened: ", e, '\n\n\n\n\n\n ****************** \n\n\n\n\n\n\n\n '
    






