import os
import commands
from contextlib import contextmanager



def get_config_file():
	print('Please enter the IP address on the server')
	print
	server_static_ip = str(raw_input('Enter server IP address: '))
	if '.' in server_static_ip and len(server_static_ip) > 8:
	
		server_config = """

# client config

# allow ntp adjustments of greater than 1000 secs
tinker panic 0


# /etc/ntp.conf, configuration for ntpd; see ntp.conf(5) for help

driftfile /var/lib/ntp/ntp.drift

# Enable this if you want statistics to be logged.
#statsdir /var/log/ntpstats/

statistics loopstats peerstats clockstats
filegen loopstats file loopstats type day enable
filegen peerstats file peerstats type day enable
filegen clockstats file clockstats type day enable


# You do need to talk to an NTP server or two (or three).
#server ntp.your-provider

server %s iburst
#server 0.ubuntu.pool.ntp.org iburst
restrict 127.0.0.1
restrict ::1
		""" % server_static_ip

		return server_config

	else:
		raise Exception('Please enter a valid IP address')

server_config = get_config_file()
server_config = server_config.split('\n')

output = commands.getoutput('ls')
print output

@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)

def update():
	os.system('sudo apt-get update')

def install_ntp():
	os.system('sudo apt-get install ntp')


def get_ip():
	output = commands.getoutput('ifconfig')
	print(output)

def write_config():
	with cd('/etc/'):
		os.system('pwd')
		config = open('ntp.conf', 'w')
		for line in server_config:
			config.write('\n')
			config.write(line)
			config.write('\n')
		config.close()

def restart():
	os.system('sudo service ntp restart')

install_ntp()
write_config()
restart()