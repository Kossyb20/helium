import os
import commands
from contextlib import contextmanager
import subprocess


@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)



try:
    home_dir = commands.getoutput('pwd').split('/')[2]
    old_version = str(raw_input('Enter current version : '))
    new_version = str(raw_input('Enter new version : '))
    with cd('/home/%s/helium' % home_dir):
        os.system('env VERSION={} docker-compose stop'.format(old_version))
        os.system('env VERSION={} docker-compose down'.format(old_version))
        os.system('env VERSION={} docker-compose up -d'.format(new_version))

except Exception as e:
    print('FATAL ERROR: ', e)