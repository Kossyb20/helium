import glob
import os
import commands
from contextlib import contextmanager
import subprocess

@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)

try:
    home_dir = commands.getoutput('pwd').split('/')[2]
    with cd('/home/%s/heliumbkp' % home_dir):
        
        list_of_files = glob.glob('/home/{}/heliumbkp/*'.format(home_dir)) 

        latest_file = max(list_of_files, key=os.path.getctime).split('/')[-1]

        print 'Backup to restore: ', latest_file

        os.system('sudo docker exec -i postgres pg_restore -U onemedical -d onemedical  -c < {}'.format(latest_file))

        print 'Successfully restored ', latest_file

except Exception as e:
    print 'FATAL ERROR: ', e