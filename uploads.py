import os
import commands
from contextlib import contextmanager
import subprocess

@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)



home_dir = commands.getoutput('pwd').split('/')[2]

try:
    root, dirs, files = os.walk("/media/{}" .format(home_dir)).next()
    drive = [i for i in dirs if not '.' in i and not ' ' in i][0]

except Exception as e:
    print e
    #print 'No Backup External Hard drive found'
    pass




def uploads_bash(path, home_dir,  drive):
    script_lines = """
#!/bin/bash
sudo crontab -l > backup_cron
echo " 0 * * * * cd {path} && sudo cp -r /home/{home_dir}/helium/uploads /media/{home_dir}/{drive}" >> backup_cron
crontab backup_cron
rm backup_cron
    """ .format(path=path, home_dir=home_dir, drive=drive)

    return script_lines.split('\n')




try:
    
    with cd('/home/%s/helium' % home_dir):
        
        path = '/home/%s/helium' % home_dir
        bsh = open('uploads_backup.sh', 'w')
        for line in uploads_bash(path, home_dir, drive):
            bsh.write('\n')
            bsh.write(line)
        bsh.close()
        os.system('sudo chmod +x uploads_backup.sh')
        os.system('./uploads_backup.sh')

    print 'Backup Setup Successfully!'

except Exception as e:
    print 'Something has gone terribly wrong!.', e

