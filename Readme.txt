Server setup:
	> Ensure internet connection on the server
	> Run 'sudo su'
	> run 'python server_setup.py'
	> Enter IP address of the server when prompted

Raspberry setup:
	> Ensure internet connection
	> Run 'sudo su'
	> run 'python raspberry_setup.py'
	> Enter IP address of the server when prompted