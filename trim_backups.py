import datetime
import os
import  commands
from contextlib import contextmanager
import time


@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)




current_date = datetime.datetime.today().date()

try:
    home_dir = commands.getoutput('pwd').split('/')[2]
    with cd('/home/%s/heliumbkp' % home_dir):
        root, dirs, files = os.walk("/home/{}/heliumbkp" .format(home_dir)).next()
        for dump in files:
            try:
                str_date = dump.split('_')[1]
                dump_date = datetime.datetime.strptime(str_date, '%d-%m-%Y' ).date()
                #print dump_date

                two_days_ago = current_date+datetime.timedelta(hours=-48)
                if dump_date < two_days_ago:
                    print dump_date
                    os.system('sudo rm {}'.format(dump))
                #print two_days_ago


            except:
                print dump

except Exception as e:
    print 'FATAL ERROR: ', e




try:
    home_dir = commands.getoutput('pwd').split('/')[2]
    root, dirs, files = os.walk("/media/{}" .format(home_dir)).next()
    drive = [i for i in dirs if not '.' in i][0]
    with cd('/media/{home_dir}/{drive}/heliumbkp' .format(home_dir=home_dir, drive=drive)):
        root, dirs, files = os.walk('/media/{home_dir}/{drive}/heliumbkp' .format(home_dir=home_dir, drive=drive)).next()
        for dump in files:
            try:
                str_date = dump.split('_')[1]
                dump_date = datetime.datetime.strptime(str_date, '%d-%m-%Y' ).date()
                #print dump_date

                two_days_ago = current_date+datetime.timedelta(hours=-240)
                if dump_date < two_days_ago:
                    print dump_date
                    os.system('sudo rm {}'.format(dump))
                    #os.system('pwd')
                #print two_days_ago


            except:
                print dump

except Exception as e:
    print 'FATAL ERROR: ', e