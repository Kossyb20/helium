import socket, struct, fcntl
import psutil
import os
import commands
from contextlib import contextmanager
import subprocess

@contextmanager
def cd(newdir):
    prevdir = os.getcwd()
    os.chdir(os.path.expanduser(newdir))
    try:
        yield
    finally:
        os.chdir(prevdir)


home_dir = commands.getoutput('pwd').split('/')[2]

def get_netmask(iface):
    return socket.inet_ntoa(fcntl.ioctl(socket.socket(socket.AF_INET, socket.SOCK_DGRAM), 35099, struct.pack('256s', iface))[20:24])


def get_ip_address(iface):
    s = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
    return socket.inet_ntoa(fcntl.ioctl(
        s.fileno(),
        0x8915,  # SIOCGIFADDR
        struct.pack('256s', iface[:15])
    )[20:24])


def get_default_gateway_linux():
    """Read the default gateway directly from /proc."""
    with open("/proc/net/route") as fh:
        for line in fh:
            fields = line.strip().split()
            if fields[1] != '00000000' or not int(fields[3], 16) & 2:
                continue

            return socket.inet_ntoa(struct.pack("<L", int(fields[2], 16)))



def get_interface_file(iface):
    print('Interface File IFACE: ', iface)
    interface_file = """

# This file describes the network interfaces available on your system
# and how to activate them. For more information, see interfaces(5).

source /etc/network/interfaces.d/*

# The loopback network interface
auto lo
iface lo inet loopback

# The primary network interface
auto {iface}
#iface {iface} inet dhcp
iface {iface} inet static
        address {ip}
        netmask {netmask}
        gateway {gateway}
        dns-nameservers 8.8.8.8
    """.format(iface=iface, ip=str(get_ip_address(iface)), netmask=str(get_netmask(iface)), gateway=str(get_default_gateway_linux()))
    interface_file = interface_file.split('\n')
    return interface_file


def get_iface():
    
    netface = ''
    interface = open('/etc/network/interfaces', 'r')
    lines = interface.readlines()
    for line in lines:
        if line[:4] == 'auto' and len(line) > 7:
            netface = line.split(' ')[-1].rstrip()
    interface.close()
    print 'IFACE: ', netface
    return netface

def write_interface():
	with cd('/etc/network/interfaces'.format(home_dir)):
		os.system('pwd')
		config = open('interfaces', 'w')
		for line in get_interface_file(get_iface()):
			config.write(line)
		config.close()


write_interface()